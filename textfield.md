# これは何？

テキスト入力が可能なUI部品。主にフォームとかダイアログに使用する。

本稿では、テキストフィールドと称す。

# 基本的な使い方

## 原則
* 発見しやすいこと
  * 目立ちやすくする
  * テキストを入力する欄であることをユーザが認識しやすくする
* 明確であること
  * 入力のステータスに応じて他のテキストフィールドとの違いが分かりやすいようにする
* 効率的であること
  * 入力に際して必要な情報を分かりやすくする
  * 入力の不備が分かりやすいようにする

## テキストフィールドの種類

以下の2種類があるため、アプリやUIのテーマにそぐうものを選んで使えばよい。
* Filled（①）
* Outlined（②）

<img src="https://material.io/design/assets/18fktKxSS5YHo2MIf3mm3VzLPoGyEUKRq/textfields-types.png" width="700px">

## 異なる種類のテキストフィールドの使い分けについて

単一のUI上で異なる種類のテキストフィールドを用いるにあたって、以下の左図のように異なる領域において異なる種類のものが使われるのは問題ないが、右図のように同一の領域において異なる種類のものが混在して使われるのは不適切である。

|OK|Bad|
|---|---|
|![](https://material.io/design/assets/1NYpRApNXh1R4lbqgr_tLl1l_Hbx_Tlrx/textfields-type-mix-do.png)|![](https://material.io/design/assets/10QziQ7saIpX7c0N6QJssXN9SEY3TG2L6/textfields-type-mix-dont.png)|


# テキストフィールドの構成要素

テキストフィールドは以下の図に示すように7つの要素によって構成されている

![](https://material.io/design/assets/1Ejie5ILk2-eAB9wBRFG93aJtYV7gJtJV/textfields-annotations-1.png)

1. Container
2. Leading icon（任意）
3. Label text
4. Input text
5. Trailing icon（任意）
6. Activation indicator
7. Assistive text（任意）

## Container

Containerはその形質を指定することによって他のテキストフィールドや周辺のコンテンツとの差別化を行い、発見のしやすさを向上させる。

* Fill and stroke
 * Filledテキストフィールドは下端にストロークを持ち、その下端ストロークの色と太さの変化はテキストフィールドがアクティブ/非アクティブの切り替えを示す
* Rounded corners
 * Outlinedテキストフィールドはすべての角、Filledなテキストフィールドは上端の角に丸みを持つ

![](https://material.io/design/assets/1M-ZaDIpRiXkZ91XAbYbIem9yIGKlJH98/textfields-anatomy-container.png)


## Label text

Label textはそのテキストフィールドに何を入力すべきをユーザに知らせるためにある。基本的にはすべてのテキストフィールドにLabel textは指定すべきである。

### 適切な使い方

Label textはテキストフィールドと協調しながら常に表示され、テキストフィールドが選択されたらフィールド上端まで移動するのが適切な挙動である。

[動画](https://material.io/design/assets/1Bna_M1j7fpIpDbNOLR1d32fjB7kARjLE/textfields-anatomy-label-text-do.mp4)

### アンチパターン
* 表示が省略されるほど長い文言をLabel textにすること
  <img src="https://material.io/design/assets/1bV3UuttijOjZTzAdGHPHJ4BlMy52HRWn/textfields-annotations-labels-dont-1.png" width="700px">
* 複数行に表示されるようなLabel textにすること
  <img src="https://material.io/design/assets/1YxkT7YL_gCx_4qQczlPkKB-zgY9_dAAq/textfields-annotations-labels-dont-2.png" width="700px">

### 入力必須のテキストフィールドについて

テキストフィールドが入力必須であることを示すには、以下のようにアスタリスク（\*）をLabel textの後に置いた上で（\*）は入力必須のテキストフィールドであることを述べる。

![](https://material.io/design/assets/1W6e4PEA6YwPK39kD8vMTUEITOI9H3BAN/textfields-annotations-required.png)

* 少数のフィールドが入力必須の場合は、以上の表示をフォームごとにやる
* 大抵のフィールドが入力必須の場合は、入力必須でないフォームごとにLabel textの後に"optional"を書き加える
* 入力必須のテキストを色付ける場合、その色が入力必須のテキストであることを示す

## Input text

以下の二つから構成される
1. Input text
2. Cursor

![](https://material.io/design/assets/13tQWdE9vsdEDbwAY9a5FI9Btb3Oae86V/textfields-annotations-2.png)

## Assistive elements

入力されたテキストに関する付加情報を提供する要素
主に以下の要素で構成される。

![](https://material.io/design/assets/1QPDRElkKlvPkRnsAivI5_xjHDcEpQigr/textfields-annotations-3.png)

1. Helper text
* 入力エリアに関する追加の案内を伝えるテキスト。原則、一行で書くものとし、常に表示するかフォーカス時に表示するかのどちらかの見た目を取る。
2. Error Message
* 入力内容が受理できないときにそのエラーに関するメッセージを表示する。原則、入力ラインの下に表示されるものとし、エラーが修正されるまでHelper textを置き換えて表示する。
3. Icons
* 主に警報的ニュアンスを伝えるのに用いる。Error Messageとの組み合わせで過剰な警報を出すが、これは主に色覚異常のユーザをターゲットにして使う。
4. Character counter
* 最大文字数と併せて、テキストエリアにあと何文字入力できるかを表示する。

### Error text

#### 適切な使い方

* エラーが一つしかない場合は、エラーを回避する方法をError textで説明する
* エラーが複数ある場合は、最も確実そうなエラーを回避する方法をError textで説明する
* error textはhelper textを置き換える形で表示する
  * [動画](https://material.io/design/assets/1-aJCEgnpvxTuvtYshKU7BuVV4EdPatjE/textfields-anatomy-error.mp4)

#### アンチパターン

* helper textの下で同時にerror textを表示すること
  * [動画](https://material.io/design/assets/1-GWRVa0l4UaElWB9CLW6k35hmUpX09g8/textfields-anatomy-error-dont.mp4)

#### 注意パターン

* 複数行のError text
  * 複数行のerror textが他のUIパーツと衝突しないように、十分な余白を取ること
  ![](https://material.io/design/assets/1WrZZKbP3jMNcVRn2VT-WGrS_9olLrZf_/textfields-anatomy-error-caution.png)


## Icons

* 付けるか否かは任意
* 主にテキストの入力手段を示したりエラーを示すのに使う
* テキストフィールド内の左端（Leading icon）か右端（Trailing icon）に付ける
* 主に以下のような使い方がある
  1. Icon signifier
   * テキストフィールドに入力されるテキストの型を示す
   * それをタッチすることでその型に応じた特別な入力コンポーネントを提供したりする
  2. Valid or error icon
   * 入力が適切であることや不適切であることを示す。Error textが色覚異常で分かりにくいようなユーザ向け
  3. Clear icon
   * テキストフィールドをクリアするボタン的な扱い
  4. Voice input icon
   * 音声入力デバイス（主にマイク）による入力
  5. Dropdown icon
   * 一覧から選択するためのボタン的な扱い

![](https://material.io/design/assets/1e2bKV4DXMixXuGWBt2lSTwlQHXq6Pq9-/textfields-annotations-4.png)



# Filledテキストフィールド

## 特徴
見た目の強調がより一層強いため、他のコンテンツやUI要素と比べて目立つという特性を持っている。

[動画](https://material.io/design/assets/1V4TTJr8n_fpZkdk4qQIqXB9vJhMegre3/textfields-filled-motion2.mp4)

## アンチパターン
ボタンと間違えるような見た目にしてはいけない

![](https://material.io/design/assets/1lyeQ5FDHka3VQgEzwegHhZt_L4cz7G1y/textfields-filled-dont.png)

## 状態一覧

6種類の状態を取り、それぞれ以下に示すような見た目になる

![](https://material.io/design/assets/1TKi-wOcOI4c_XQswgLFfHS1078d1wAzJ/textfields-filled-states.png)

# Outlinedテキストフィールド

## 特徴
Filledテキストフィールドと比べて見た目の強調は弱く、他にも多くのテキストフィールドが置かれるようなシチュエーション（フォームなど）においてシンプルなレイアウトであるように仕立て上げられる。

[動画](https://material.io/design/assets/1lqW9hDYVzYAVXnOtAHlVh4E9o09vK5oZ/textfields-outline-motion1.mp4)

## 状態一覧

6種類の状態を取り、それぞれ以下に示すような見た目になる

![](https://material.io/design/assets/1DxDXQRkLemKRP1vsos0kJC-ET1ibXY2I/textfields-outline-states-enabled.png)

# 入力形式の種類について

テキストフィールドには以下の3種類の入力形式がある

* Single line text fields
  * 単一行で固定
  * 長いテキストを扱うには不適切
  * [動画](https://storage.googleapis.com/spec-host-backup/mio-design%2Fassets%2F1Wp0W2DNhQfZq3bhLq7EDSvMXhi0y8kwr%2Ftextfields-input-motion2.mp4)
* Multi-line text fields
  * 複数行のテキストが入力可能
  * 入力テキストが改行されるたびにフィールドが下に広がることで、テキストが省略されずに表示される
  * [動画](https://storage.googleapis.com/spec-host-backup/mio-design%2Fassets%2F1nsI20VAObijg3UMEv1JoKrV_rzRc8dsJ%2Ftextfields-input-multiline.mp4)
* Text areas
  * 複数行のテキストが入力可能
  * 縦の長さは固定で、入力テキストがエリア内に収まらない場合はカーソルの位置に応じてスクロールする
  * [動画](https://storage.googleapis.com/spec-host-backup/mio-design%2Fassets%2F1p3r_LfcfFF_m3zp5oYa419ZY5bfgiMld%2Ftextfields-input-textareas.mp4)

# Androidで使う方法

* Android Design Support LibraryにTextInputLayoutを使うことで上記のテキストフィールドUIを実現できる
  * https://material.io/develop/android/components/text-input-layout/
